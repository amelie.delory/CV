<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0">
    <title>Amélie DELORY - Site professionnel</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="menu">
    <ul class="accueil">
        <li><a href="#ancre_parcours">Parcours</a></li>
        <li><a href="#ancre_competences">Compétences</a></li>
        <li><a href="#ancre_experience">Expérience</a></li>
        <li><a href="#ancre_divers">Divers</a></li>
        <li><a href="#ancre_contact">Contact</a></li>
    </ul>
</div>
<div id="container">
    <div id="infos">
        <h1>Amélie DELORY | Formation - Développement Web</h1>

        <p>
            24 ans
            -
            Permis B, véhiculée
        </p>
        <span id="ancre_parcours"></span>
    </div>
    <div>
        <h2 class="grandstitres">Parcours</h2>

        <div class="divparcours">

            <ul id="horizontal_list">
                <li>
                    Bac L <br>
                    Option Anglais
                </li>
                <li class="li_margin_left">
                    1ère année LEA <br>
                    Anglais - Chinois
                </li>
                <li class="li_margin_left">
                    BTS<br>
                    Assistant de Manager
                </li>
            </ul>
            <img src="images/flechehorizontale.jpg">
        </div>
        <span id="ancre_competences"></span>
    </div>
    <div>
        <h2 class="grandstitres">
            Domaines de compétence
        </h2>
        <ul class="competences">
            <li>HTML et CSS en autodidacte (ex : le présent site)</li>
            <li>Évaluer les besoins dans l'entreprise</li>
            <li>Organiser des événements</li>
            <li>Informer</li>
            <li>Communiquer</li>
            <li>Traiter la domiciliation commerciale</li>
            <li>Réaliser des bilans pédagogiques</li>
            <li>Rédiger des procès-verbaux</li>

        </ul>
        <ul class="competences" id="competences_exception">
            <li> Trier et rédiger divers courriers</li>
            <li>Répondre à une demande de candidature</li>
            <li>Réaliser des rapports expertise</li>
            <li>Prendre en charge le standard téléphonique et l'accueil</li>
            <li>Gérer les agendas</li>
            <li>Réaliser une plaquette de tarifs</li>
            <li>Faire les inventaires</li>
        </ul>
        <div style="clear:both;"></div>
    </div>
    <div class="bureautiquelangue">
        <h3>
            Bureautique
        </h3>
        <ul class="listehorizontale">
            <li>Word</li>
            <li>Access</li>
            <li>Excel</li>
            <li>Powerpoint</li>
            <li>FreeMind</li>
        </ul>
    </div>
    <div class="bureautiquelangue">
        <h3>Langues</h3>

        <ul class="listehorizontale">
            <li>Anglais - Très bon</li>
            <li>&bull;</li>
            <li>Allemand - Notions</li>
        </ul>
    </div>
    <div style="clear:both;"></div>
    <span id="ancre_experience"></span>
    <h2 class="grandstitres">
        Expérience professionnelle
    </h2>
    <div class="experience" id="année">
        <ul class="listetravail">
            <li>Oct. 2014 - Déc. 2015</li>
            <li>Septembre 2014</li>
            <li>Depuis 2014</li>
            <li>Nov. - Déc. 2013</li>
            <li>Mai - Juin 2013</li>
        </ul>
    </div>
    <img class="experience" src="images/flecheverticale.jpg">

    <div class="experience">
        <ul class="listetravail">
            <li>Intérimaire au Tri Postal d'Arras (horaires de nuit)</li>
            <li>Vendangeuse dans la région Rhône-Alpes</li>
            <li>Remplaçante de la secrétaire à l'Orthopédie Meneghetti à Beaurains (62)</li>
            <li>Stage de 6 semaines à la mairie d'Arras - Service des Relations Internationales</li>
            <li>Stage de 6 semaines chez ABL Secrétariat à Arras</li>
        </ul>
    </div>
    <div style="clear:both;"></div>
    <span id="ancre_divers"></span>
    <h2 class="grandstitres">Divers</h2>

    <div id="badminton_hold">
        <img class="divers" src="images/bad.jpg">

        <p class="divers" id="badminton">
            Badminton en compétition depuis 6 ans
        </p>
    </div>
    <div>
        <img class="divers" src="images/musique.jpg">
        <ul class="divers">
            <li> Productrice en MAO (Musique Assistée par Ordinateur)</li>
            <li id="withoutIcon"> &#8658; <a href="https://soundcloud.com/key-brain-chemical"> Cliquez ici pour
                    m'écouter ! </a></li>
            <li>Guitare et chant depuis 10 ans</li>
            <li> Passion pour la scène locale et les concerts live</li>
        </ul>
        <span id="ancre_contact"></span>
    </div>
    <div style="clear:both;"></div>
    <h2 class="grandstitres">Contact</h2>
    <div id="contact">
        <form method="POST" action="/">
            <label for="prenom">Prénom : </label><input class="text_field" id="prenom" type="text" name="prenom" placeholder="Saisissez ici votre prénom"/><br/>
            <label for="nom">Nom : </label><input class="text_field" type="text" name="nom" id="nom" placeholder="Saisissez ici votre nom"/><br/>
            <label for="email">Email : </label><input class="text_field" type="email" name="email" id="email" placeholder="Saisissez ici votre email"/><br/>
            <label for="contenu">Votre message : </label><br/>
            <textarea id="contenu" name="contenu" rows="5" cols="58"></textarea><br/><br/>
            <input id="submit" type="submit" name="mail_contact" value="Envoyer le mail">
        </form>
    </div>
    <?php
    if(isset($_POST['mail_contact'])) {
        if (!empty($_POST['email']) && !empty($_POST['contenu']) && isset($_POST['nom']) && isset($_POST['prenom']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $destinataire = "amelie.delory@hotmail.fr";
            $contenu = htmlspecialchars($_POST['contenu']);
            $prenom = htmlspecialchars($_POST['prenom']);
            $nom = htmlspecialchars($_POST['nom']);
            $email = htmlspecialchars($_POST['email']);
            $message = "Nom : " . $nom
                . "<br/>"
                . "Prénom : " . $prenom
                . "<br/>"
                . "Email : " . $email
                . "<br/><br/>"
                . "Message : <br/>"
                . $contenu;
            $headers = "From: " . $email ."\r\n";
            $headers .= "Reply-To: ". $email ."\r\n";
            $headers  .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            mail($destinataire, "Formulaire de contact - amelie-delory.fr", $message, $headers);
            echo "<script type='text/javascript'>alert('L\'email a été correctement envoyé. Merci.')</script>";
        } else {
            echo "<script type='text/javascript'>alert('Une erreur s\'est produite, veuillez vérifier les données saisies.')</script>";
        }
    }
    ?>
    <div id="margin_bottom_page"></div>
    <p id="copyright">
        Copyright Amélie Delory - All rights reserved - 2015
    </p>
</div>
</body>
</html>